require('../db_init/mongoose')
const User = require('../models/users')

//Exmple for promise chaining 
// User.findByIdAndUpdate("5ebd574ac5441f1138b69d21",{age:1}).then((user)=>{
//     console.log(user)
//     return User.countDocuments({age:1})
// }).then((count)=>{
//     return console.log(count);
    
// })
// .catch((e)=>{
//     console.log("Something went wrong"+e);
    
// })

//below we are using promise chaining
 updateUserAge = async (id,age)=>{
    const user = await User.findByIdAndUpdate(id,{age})
    const count = await User.countDocuments({age})
    return count;
 }

 const res = updateUserAge("5ebd574ac5441f1138b69d21",22).then((result)=>{
     console.log(result);
     
 }).catch((e)=>{
    console.log(e)
 })