require('../db_init/mongoose')
const Task = require('../models/task')

//Exmple for promise chaining 
// User.findByIdAndUpdate("5ebd574ac5441f1138b69d21",{age:1}).then((user)=>{
//     console.log(user)
//     return User.countDocuments({age:1})
// }).then((count)=>{
//     return console.log(count);
    
// })
// .catch((e)=>{
//     console.log("Something went wrong"+e);
    
// })

//below we are using promise chaining
deleteTask = async (id)=>{
    const delTask = await Task.findByIdAndDelete(id)
    const count = await Task.countDocuments({"status":true})
    return count
}

const res = deleteTask("5ebbe03bb48d524e10abc4eb").then((result)=>{
    console.log(result)
}).catch((e)=>{
    console.log(e)
})
 