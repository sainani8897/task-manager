const express = require('express')
require('./db_init/mongoose')

const User = require('./models/users')
const Task = require('./models/task')
const app = express()
const port = process.env.PORT || 3000

app.use(express.json())

app.get('/',(req,res)=>{
    return res.send({
        code:200,
        message:"Success"
    })   
})


//Old code without async and await
app.get('/users',(req,res)=>{ 
    const user = User.find({})
                    .then((user)=>{
                        res.send({
                            code:200,
                            data:user
                        })
                    })
                    .catch((error)=>{
                        res.send(error)
                    })


})

app.get('/users/:id',(req,res)=>{
    const _id = req.params.id
    const user = User.findById(_id)
                        .then((user)=>{
                            if(!user){
                                return res.status(404).send({
                                    code:404,
                                    message:"Not found!"
                                })
                            }
                            else{
                                res.send({
                                    code:200,
                                    data:user
                                })
                            }
                           
                        })
                        .catch((error)=>{
                            res.status(500).send(error)
                        })
})



app.post('/users',(req,res)=>{
    const user = new User(req.body).save()
                    .then((user)=>{
                        res.status(201).send({
                            code:201,
                            message:"Created Successfully"
                        })
                    })
                    .catch((error)=>{
                        res.send(error)
                    })
})


app.get('/tasks',(res,req)=>{
    const task =  User.find({})
                    .then((user)=>{
                        res.send({
                            code:200,
                            data:tasks
                        })
                    })
                    .catch((error)=>{
                        res.status(500).send()
                    })
})

app.post('/tasks',(req,res)=>{
    const task = new Task(req.body).save()
                    .then((task)=>{
                        res.status(201).send({
                            code:201,
                            message:"Created Successfully"
                        })
                    })
                    .catch((error)=>{
                        res.send(error)
                    })
})

app.listen(3000,()=>{
    console.log('Sever is running at port 3000');
    
})