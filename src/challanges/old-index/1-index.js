const express = require('express')
require('./db_init/mongoose')

const User = require('./models/users')
const Task = require('./models/task')
const app = express()
const port = process.env.PORT || 3000

app.use(express.json())

app.get('/',(req,res)=>{
    return res.send({
        code:200,
        message:"Success"
    })   
})

//app.get('/users',(req,res)=>{
    // Old code without async and await
    // const user = User.find({})
    //                 .then((user)=>{
    //                     res.send({
    //                         code:200,
    //                         data:user
    //                     })
    //                 })
    //                 .catch((error)=>{
    //                     res.send(error)
    //                 })


//})
app.get('/users',async(req,res)=>{
    try {
        const user = await User.find({})
        return res.send({user})
    } catch (error) {
        return res.status(400).send(error)
    }
})

app.get('/users/:id',async(req,res)=>{
    try {
        const id = req.params.id
        const user = await User.findById(id)
        return res.status(201).send({user})  
    } catch (error) {
        return res.status(500).send(error)
    }
})

app.patch('/users/:id',async(req,res)=>{
    try {
        const id = req.params.id
        const updates = Object.keys(req.body)
        const allowUpdate = ["name","password","age","email"]
        const isValidOperation = updates.every((update)=>allowUpdate.includes(update))
        if(!isValidOperation)
        {
            return res.status(400).send()
        }
        const user = await User.findByIdAndUpdate(id,req.body,{new:true,runValidators:true})
        if(!user){
            return res.status(404).send()
        }   
        return res.status(201).send({user})  
    } catch (error) {
        return res.status(500).send(error)
    }
})

app.post('/users',async (req,res)=>{
    try {
        const request = req.body
        const user = await new User(request).save()
        return res.status(201).send({
            code:201,
            data:user
        })
    } catch (error) {
        return res.status(400).send(error)
    }
})


app.get('/tasks',async (req,res)=>{
    try {
        const tasks =  await Task.find({})
        return res.send({
            code:200,
            data:tasks
        })
    } catch (error) {
        return res.status(500).send(error)
    }

})

app.post('/tasks', async (req,res)=>{
    try {
        const task = await new Task(req.body).save()
        return res.send(task)    
    } catch (error) {
        return res.status(400).send(error)   
    }
    
})

app.listen(3000,()=>{
    console.log('Sever is running at port 3000');
    
})