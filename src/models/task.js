const mongoose = require('mongoose')
const validator = require('validator')
const User = require('../models/users')

const taskSchema = new mongoose.Schema({
    title:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    status:{
        type:Boolean,
        default:false
    },
    created_by:{
       type:mongoose.Schema.Types.ObjectId,
       required:true,
       ref:'User'
    }
},{
    timestamps:true
})

const Task = mongoose.model('Task',taskSchema)
module.exports = Task