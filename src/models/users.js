const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const Task = require('../models/task')


//using laravel kind of schema
const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    age:{
        type:Number,
    },
    email:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        lowercase:true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error("Not a valid Email")
            }
        }
    },
    password:{
        type:String,
        required:true,
        trim:true,
        validate(value){
            if(value.length<8){
                throw new Error('Password length is min 8')
            }
        }
    },
    country:{
        type:String
    },
    tokens:[{
        token:{
            type:String,
            required:true
        }
    }],
    profile_pic:{
        type: Buffer
    },
},{
    timestamps:true
})

//Just like wirting hasMany relation in Laravel 
userSchema.virtual('tasks',{
    ref:'Task',
    localField:'_id',
    foreignField:'created_by'
})

// accessing userSchema
//here we are using normal function of usage of this
userSchema.pre('save', async function(next){
    const user = this // you can user this directly or save it another variable use with that variable
   if(user.isModified('password')){
       user.password = await bcrypt.hash(user.password,8)
   }

   next()

})


userSchema.methods.toJSON = function () {
    const user = this
    const userObject = user.toObject()
    delete userObject.password
    delete userObject.tokens
    return userObject
}

userSchema.statics.findByCredentials = async (email,password) =>{
    const user = await User.findOne({email})
    if(!user){
        throw new Error("This user does not exits in our database!")
    }

    const isMatch = await bcrypt.compare(password,user.password)

    if(!isMatch){
        throw new Error("Invalid logins!")
    }

    return user;
}


userSchema.methods.generateAuthToken = async function () {
    const user = this
    const token = jwt.sign({_id:user._id.toString()},process.env.JWT_KEY) //generatin a token
    user.tokens = user.tokens.concat({token}) // geting user tokens and concat the new token 
    await user.save() // saving the user tokens in his document
    return token
}




const User = mongoose.model('User',userSchema)

module.exports = User