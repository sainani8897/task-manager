const sgMail = require('@sendgrid/mail');
const sendGridApiKey = process.env.SENDGRID_API_KEY;
sgMail.setApiKey(sendGridApiKey);
const msg = {
  to: 'sainath@digitohub.com',
  from: 'sainath.dekonda@dcodelabs.in',
  subject: 'Welcome  to Taskmanage App',
  text: 'and easy to do anywhere, even with Node.js',
};
const sendEmail = (email,message) =>{
  msg.to = email
  msg.text = message

  sgMail.send(msg);
}


module.exports = sendEmail