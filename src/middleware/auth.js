// const express = require('express')
const jwt = require('jsonwebtoken')
const User = require('../models/users')
// const app = new express.Router
// app.use((req,res,next)=>{
//     res.status(503).send({
//         code:503,
//         message:"Site is under maintaince for 2pm please try after sometime"
//     })
// })

const auth = async (req,res,next)=>{
    try {

    const token = req.header("Authorization").replace("Bearer ","")
    const decode = jwt.verify(token,process.env.JWT_KEY)
    const user = await User.findOne({_id:decode._id,'tokens.token':token})

    if(!user){
        throw new Error("Authorisation Failed")
    }
    req.user = user
    req.token = token
    next()
    } catch (error) {
        console.log(error)
         res.status(401).send({
             error:"Unauthorized user"
         })   
    }
}



module.exports = auth