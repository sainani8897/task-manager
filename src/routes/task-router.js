const express = require('express')
require('../db_init/mongoose')
const auth = require('../middleware/auth')
const Task = require('../models/task')
const router = new express.Router


router.get('/tasks',async (req,res)=>{
    try {
        const tasks =  await Task.find({})
        return res.send({
            code:200,
            data:tasks
        })
    } catch (error) {
        return res.status(500).send(error)
    }

})

router.get('/tasks/:id',auth,async (req,res)=>{
    try {
        const id = req.params.id
        const tasks =  await Task.findById(id)
        //Belongs to Relation
        await tasks.populate('created_by').execPopulate()
        console.log(tasks.created_by);
        return res.send({
            code:200,
            data:tasks
        })
    } catch (error) {
        return res.status(500).send(error)
    }

})

router.get("/users-tasks",auth, async (req,res)=>{
    try {
       const user = req.user
       const match = {}
       const sort = {}
       if(req.query.status){
           match.status = req.query.status==="true"?true:false
       }

       if(req.query.sortBy){
           const parts = req.query.sortBy.split(":")
           sort[parts[0]] = parts[1]==='desc'?-1:1 
       }
       //filtering the data
        await req.user.populate({
            path:"tasks",
            match:match,
            options:{
                limit:parseInt(req.query.limit),
                skip:parseInt(req.query.skip),
                sort:sort
            }
        }).execPopulate()
        tasks = user.tasks
        return res.send({
            code:200,
            data:tasks
        })
    } catch (error) {
        return res.status(400).send(error)   
    }
})

router.post('/tasks',auth, async (req,res)=>{
    try {
        const task = await new Task({
            ...req.body,
            created_by:req.user._id
        }).save()
        return res.send(task)    
    } catch (error) {
        return res.status(400).send(error)   
    }
    
})

module.exports = router
