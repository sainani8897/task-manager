const express = require('express')
const multer = require('multer')
require('../db_init/mongoose')
const sharp = require('sharp')


const User = require('../models/users')
const auth = require('../middleware/auth')
const app = new express.Router
const sendEmail = require('../emails/send')


app.get('/users',auth,async(req,res)=>{
    try {
        const user = await User.find({})
        return res.send({user})
    } catch (error) {
        return res.status(400).send(error)
    }
})

app.get('/users/current',auth,async(req,res)=>{
    try {
        return res.send(req.user)
    } catch (error) {
        return res.status(400).send(error)
    }
})

app.get('/users/:id',async(req,res)=>{
    try {
        const id = req.params.id
        const user = await User.findById(id)
        return res.status(201).send({user})  
    } catch (error) {
        return res.status(500).send(error)
    }
})

// Update me
app.patch('/users',auth,async(req,res)=>{
    const updates = Object.keys(req.body)
    const allowUpdate = ["name","password","age","email"]
    const isValidOperation = updates.every((update)=>allowUpdate.includes(update))
    if(!isValidOperation)
    {
        return res.status(400).send()
    }
    try {
        //old version an new change is required for password Middleware
        // const user = await User.findByIdAndUpdate(id,req.body,{new:true,runValidators:true})
        const user = req.user

        updates.forEach((update)=>{
            user[update] = req.body[update] // here we are using bracker notation just like $keys in php
        })

        await user.save()
        
        return res.status(201).send({user})  
    } catch (error) {
        return res.status(500).send(error)
    }
})

app.post('/users',async (req,res)=>{
    try {
        const request = req.body
        const user = await new User(request).save()
        sendEmail(user.email,'Welcome to Dcodelabs.in ${user.name}')
        const token = await user.generateAuthToken()
        return res.status(201).send({
            code:201,
            data:{user,token}
        })
    } catch (error) {
        // console.log(error);
        return res.status(400).send(error)
    }
})


app.post("/login",async(req,res)=>{
    try {
        const user = await User.findByCredentials(req.body.email,req.body.password)
        const token = await user.generateAuthToken()
        return  res.send({user,token})  
    } catch (error) {
       return res.status(400).send({error})
    }
})

app.post('/logout',auth,async(req,res)=>{
    try {
        req.user.tokens = req.user.tokens.filter((token)=>{
            return token.token !== req.token
        })   
        await req.user.save()
        res.send({
            code:200,
            message:"Logged out successfully"
        })
    } catch (error) {
        return res.status(500).send({error})
    }
})

app.post('/logout-all',auth,async(req,res)=>{
    try {
        req.user.tokens = []
        await req.user.save()
        res.send({
            code:200,
            message:"Logged out successfully"
        })
    } catch (error) {
        return res.status(500).send({error})
    }
})

const upload = multer({
    // dest:"public/images",
    limits:{
        fileSize:2000000
    },
    fileFilter(req, file, callback){
        console.log(file.originalname)
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
            return callback(new Error("Not valid image format"))
        }
        callback(undefined,true)
    }

})

app.post('/user/profile-pic',auth,upload.single('upload'),async (req,res)=>{
    //**  old image upload
    // req.user.profile_pic = req.file.buffer\
    // ** new Image upload
    req.user.profile_pic = await sharp(req.file.buffer).resize({width:250,height:250}).png().toBuffer()
    await req.user.save()
    res.send({
        code:200,
        message:"Success"
    })
},(error,req,res,next)=>{
    res.status(400).send({
        code:400,
        message:error.message
    })
})

app.delete('/user/delete-profile-pic',auth,async(req,res)=>{
    req.user.profile_pic = undefined
    await req.user.save()
    res.send({
        code:200,
        message:"Success"
    })
})

app.get('/user/:id/profile-pic',async(req,res)=>{
    try {
        const id = req.params.id
        const user = await User.findById(id)        
        if(!user || !user.profile_pic){
            throw new Error("No user found!")
        }
        res.set("Content-Type",'image/png')
        return res.send(user.profile_pic)  
    } catch (error) {
        return res.status(500).send(error)
    }
})


module.exports = app

