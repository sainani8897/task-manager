const express = require('express')
const multer = require('multer')
require('./db_init/mongoose')


const user = require('./routes/user-router')
const tasks = require('./routes/task-router')
const app = express()
const port = process.env.PORT

app.use(express.json())
app.use(user,tasks)

app.get('/',(req,res)=>{
    return res.send({
        code:200,
        message:"Success"
    })   
})

const upload = multer({
    dest:"public/images"
})
app.post('/test/upload',upload.single('upload'),(req,res)=>{
    res.send({
        code:200,
        message:"Success"
    })
})



app.listen(port,()=>{
    console.log('Sever is running at port '+port);
    
})