//Insert data in mongo db

const task = db.collection("tasks").insertMany([
    {
        title:"Deploy dcodelabs.in live",
        description:"Deployment of dcodelabs.in website in aws server",
        complted:true
    },
    {
        title:"Learn nodejs form udemy",
        description:"Learn node.js from ",
        status:false
    },
    {
        title:"Aws Cetrtification",
        description:"Do aws certification program and complete the task given.x",
        status:false
    }
],(error,result)=>{
    if(error){
        return console.log('Oops something went wrong!');
    }
    console.log(result.insertedIds);
})


const findOne = db.collection('tasks').findOne({_id: new ObjectId("5ebbe03bb48d524e10abc4eb")},(error,result)=>{
    if (error) {
        return console.log('Error!')
    }
    console.log(result);

})

const find = db.collection('tasks').find({status:false}).toArray((error,result)=>{
    if(error){
        return console.log("Error!");
    }
    console.log(result);  
})


const updateMany = db.collection('tasks').updateMany({status:false},{
    $set:{
        status:true
    }
}).then((result)=>{
    console.log(result);
}).catch((error)=>{
    console.log(error);
})


const deleteOne = db.collection('tasks').deleteOne({_id:new ObjectId("5ebbe03bb48d524e10abc4eb")})


