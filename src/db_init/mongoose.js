const mongoose = require('mongoose')

mongoose.connect(process.env.MONGO_DB_URL,{
    useUnifiedTopology: true,
    useCreateIndex:true,
    useNewUrlParser: true,
    useFindAndModify:false
    
})



// const user = new User({
//     name:"Vishwanath",
//     age:22,
//     country:"India"
// }).save().then((user)=>{
//     console.log(user);
    
// }).catch((error)=>{
//     console.log(error);
    
// })
